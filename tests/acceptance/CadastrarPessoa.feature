#enconding: utf-8
#language: pt


@Cadastrar_Usuario
Funcionalidade: Cadastrar_Pessoa_Fisica
Como usuario do sistema quero cadastrar uma pessoa no sistema

Contexto:
Dado que tenha acessado com perfil "ADV_1G" no ambiente "1G_RS"
E que tenha acessado o menu "Usuários/Cadastro de Usuários"

Cenário: Cadastrar_Pessoa_Fisica
Quando insiro um cpf valido e consulto
E confirmo o novo cadastro
E insiro os dados da pessoa
#Então pessoa cadastrado com sucesso