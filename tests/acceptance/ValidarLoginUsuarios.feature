#enconding: utf-8
#language: pt

@Login
Funcionalidade: Login
Como usuário do e-proc 
Eu quero acessar o sistema
Para exercer minhas atividades


Esquema do Cenário: Acessar o sistema
Dado que tenha logado no sistema no ambiente "<ambiente>" usando o usuario "<usuario>" e senha "<senha>"
E seleciono o perfil "<perfil>"
Então devo ver a tela "<tela>"

Exemplos:
| ambiente | usuario  | senha   | perfil   | tela               |
| 1G_RS | RS240977 | senha2G | RS240977 | Painel do Advogado |
| 1G_RS | servsec | Homolog2 | 1ª Vara Cível do Foro Regional 4º Distrito da Comarca de Porto Alegre   (POA4D01) | Painel do Servidor   |
| 1G_RS | magistrado | Homolog1 | 1ª Vara Judicial da Comarca de Encantado   (ETD01) | Painel do Magistrad   |
| 1G_RS | admineproc | Homolog696 |  | Painel do Administrador |
| 1G_RS | RS240977 | senha2G | RS240977 | Painel do Advogado |