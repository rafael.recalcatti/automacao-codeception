#enconding: utf-8
#language: pt


@Cadastrar_Petição_Inicial
Funcionalidade: Cadastrar_Petição_Inicial
Como representante da parte
Eu quero cadastrar uma petição inicial no 1º Grau
Para distribuição de um processo

Contexto:
Dado que tenha acessado com perfil "Advogado" no ambiente "2G_RS"
E que tenha acessado o menu "Petição Inicial"

Esquema do Cenário: Cadastrar Petição Inicial
Quando cadastro com a classe "<classe>" na competencia "Família" com assunto "<assunto>"
Então devo ver o processo distribuido

Exemplos:
| classe | assunto |
| ALIENAÇÃO JUDICIAL DE BENS | teste |
