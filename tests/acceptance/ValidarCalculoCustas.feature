#enconding: utf-8
#language: pt


@Validar_Calculo_Custas
Funcionalidade: Validar_Calculo_Custas
Como representante da parte
Eu quero verificar o calculo de custas diferenciado para cada classe de processo

Contexto:
Dado que tenha acessado com perfil "Advogado" no ambiente "1gteste"
E que tenha acessado o menu "Petição Inicial"

Esquema do Cenário: Validar Calculo Custas
Quando cadastro a petição inicial de "<classeProcessual>" ate a etapa "1"
Então devo receber o valor de custas de "<valorCustas>"

Exemplos:
| classeProcessual | valorCustas |
| Alimentos | Não há custas |
| Alienação Bens | R$ 1000.00 |
| Sobrepartilha | R$ 250.00 |
#| COMPETENCIA FAMILIA | Não há custas |
#| COMPETENCIA FAMILIA MANDADO | R$ 146.10 |

