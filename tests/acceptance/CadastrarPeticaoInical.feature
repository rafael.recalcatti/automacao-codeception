#enconding: utf-8
#language: pt


@Cadastrar_Petição_Inicial
Funcionalidade: Cadastrar_Petição_Inicial
Como representante da parte
Eu quero cadastrar uma petição inicial no 1º Grau
Para distribuição de um processo

Contexto:
Dado que tenha acessado com perfil "ADV_RS" no ambiente "2G_RS"
E que tenha acessado o menu "Petição Inicial"

Cenário: Cadastrar Petição Inicial
Quando cadastro a petição inicial de "PI_FAM" ate a etapa "5"
Então devo ver o processo distribuido