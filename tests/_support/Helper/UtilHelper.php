<?php
namespace Helper;


use Codeception\Exception\ModuleConfigException;
use Codeception\Module as CodeceptionModule;
use Codeception\Module\Db;
use Codeception\Module\WebDriver;
use Symfony\Component\Yaml\Yaml;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class UtilHelper extends CodeceptionModule
{
    public function __construct()
    {
        
    }

    /**
     * Cria uma borda no elemento com a cor(string,rgb,hexadecimal) passada como parâmetro
     * @param $actor
     * @param $element
     * @param $color
     * @return void
     */
    public function higthline($actor, $element, $color = 'GREEN')
    {
        switch (substr($element, 0, 1)) {

            case '#':
                $tipoLocator = 0;
                break;

            case '.':
                $tipoLocator = 1;
                break;

            default:
                $tipoLocator = 2;
                break;

        }

        $element = (self::startWith($element, '.')) ? str_replace(".", "", $element) : str_replace("#", "", $element);

        try
        {

            switch ($tipoLocator) {

                case 0:
                    $actor->executeJS("document.getElementById('" . $element . "').style.border='3px solid " . $color . "';");
                    break;

                case 1:
                    $actor->executeJS("document.getElementsByClassName('" . $element . "')[0].style.border='3px solid " . $color . "';");
                    break;

                case 2:
                    $actor->executeJS("document.evaluate('" . $element . "', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.style.border='3px solid " . $color . "';");
                    break;
            }

        } catch (\Throwable | \Error | \Exception $e) {}

    }

    public function setValueJS($actor, $element, $value)
    {
        switch (substr($element, 0, 1)) {

            case '#':
                $tipoLocator = 0;
                break;

            case '.':
                $tipoLocator = 1;
                break;

            default:
                $tipoLocator = 2;
                break;

        }

        $element = (self::startWith($element, '.')) ? str_replace(".", "", $element) : str_replace("#", "", $element);

        try
        {

            switch ($tipoLocator) {

                case 0:
                    $actor->executeJS("document.getElementById('" . $element . "').value = " . $value);
                    break;

                case 1:
                    $actor->executeJS("document.getElementsByClassName('" . $element . "')[0].value = " . $value);
                    break;

                case 2:
                    $actor->executeJS("document.evaluate('" . $element . "', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value = " . $value);
                    break;
            }

        } catch (\Throwable | \Error | \Exception $e) {}

    }

    /**
     * Efetua o click via JS no elemeto passado como parâmetro
     * @param $actor
     * @param $element
     * @return void
     */
    public function clickJS($actor, $element)
    {

        switch (substr($element, 0, 1)) {

            case '#':
                $tipoLocator = 0;
                break;

            case '.':
                $tipoLocator = 1;
                break;

            default:
                $tipoLocator = 2;
                break;

        }

        $element = (self::startWith($element, '.')) ? str_replace(".", "", $element) : str_replace("#", "", $element);

        try
        {
            switch ($tipoLocator) {

                case 0:
                    $actor->executeJS("document.getElementById('" . $element . "').click();");
                    break;

                case 1:
                    $actor->executeJS("document.getElementsByClassName('" . $element . "')[0].click();");
                    break;

                default:
                    $actor->executeJS("document.evaluate('" . $element . "', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()");
                    break;
            }

        } catch (\Throwable | \Error | \Exception $e) {}

    }
    /**
     * Retorna o diretorio atual da classe onde se encontra o método, pode ser retornar na árvore
     * passando a quantidade de retornos como parâmentro
     * @param $numberReturns
     * @return string
     */
    public static function getDirectoryCurrent($numberReturns = 0)
    {
        $actualDirectorio = __DIR__;

        for ($i = 0; $i < $numberReturns; $i++) {
            $actualDirectorio = dirname($actualDirectorio);
        }
        return $actualDirectorio;
    }
    /**
     * Remove acentos da string passada como paâmetro
     * passando a quantidade como parâmentro
     * @param $str
     * @return string
     */
    public function removeAccent($str)
    {

        $arrFromUp = array("Á", "À", "Â", "Ã", "Ç", "É", "È", "Ê", "Í", "Ì", "Ó", "Ò", "Ô", "Õ");
        $arrToUP = array("A", "A", "A", "A", "C", "E", "E", "E", "I", "I", "O", "O", "O", "O");

        $arrFromLow = array("á", "à", "â", "ã", "ç", "é", "è", "ê", "í", "ì", "ó", "ò", "ô", "õ");
        $arrToLow = array("a", "a", "a", "a", "c", "e", "e", "e", "i", "i", "o", "o", "o", "o");

        return str_replace($arrFromLow, $arrToLow, str_replace($arrFromUp, $arrToUP, $str));
    }

    /**
     * Verifica se a string começa com o caracter passado como parâmetro
     * @param $str
     * @param $caracter
     * @return bool
     */
    public function startWith($str, $caracter, int $index = 1)
    {

        if (strlen($str) == 0) {
            throw new Exception('$str não pode ser null/void');
        } else if (strlen($caracter) == 0) {
            throw new Exception('$caracter não pode ser null/void');
        } else {
            return substr(strtoupper($str), 0, $index) == strtoupper($caracter);
        }

    }

    /**
     * Verifica se os atributos a da classe page são os mesmos do arquivo de massas .YML
     * @param $class
     * @param $arrayDados
     * @return void
     */
    public function validateAttributesPage($class, $arrayDados, $archive)
    {

        $classArray = get_class_vars(get_class($class));
        unset($classArray['moduleContainer'], $classArray['storage'], $classArray['config'], $classArray['backupConfig'], $classArray['requiredFields'], $classArray['includeInheritedActions'], $classArray['excludeActions'], $classArray['aliases'], $classArray['onlyActions']);
        
        foreach ($arrayDados as $k => $v) {

            if (!array_key_exists($k, $classArray)) {

                throw new ModuleConfigException(get_class($class), sprintf(
                    'O atributo "%s" do arquivo "%s" não esta definido na classe "%s".',
                    $k, $archive, get_class($class)
                ));

            }

        }

        foreach ($classArray as $k => $v) {

            if (!array_key_exists($k, $arrayDados)) {

                throw new ModuleConfigException(get_class($class), sprintf(
                    'O atributo "%s" da classe "%s" não esta definido no arquivo "%s".',
                    $k, get_class($class), $archive
                ));

            }

        }

    }

    public function mask($mask, $str)
    {

        $str = str_replace(" ", "", $str);

        for ($i = 0; $i < strlen($str); $i++) {
            $mask[strpos($mask, "#")] = $str[$i];
        }

        return $mask;

    }
    
    public function loadXMLResult(){

        $dir_xml = self::getDirectoryCurrent(2).'\_output\report.xml';
        $xml = new \SimpleXMLElement($dir_xml, 0, true);        

        $array = [];
        foreach ($xml->children() as $node) {
            $nodeName = $node->getName();
            $isArray = is_array($node);
            $thisArray = $xml[$node];
            $array[$node->getName()] = is_array($node) ? simplexml_to_array($node) : (string) $node;
        }
        
        print_r($array);

    }
    /**
     * Método para gerar CNPJ válido, com máscara ou não
     * @example cnpjRandom(0) (para retornar CNPJ sem máscara)
     * @param int $mascara
     * @return string
     */
    public function cnpjRandom($mascara = "1")
    {
        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
        $n9 = 0;
        $n10 = 0;
        $n11 = 0;
        $n12 = 1;
        $d1 = $n12 * 2 + $n11 * 3 + $n10 * 4 + $n9 * 5 + $n8 * 6 + $n7 * 7 + $n6 * 8 + $n5 * 9 + $n4 * 2 + $n3 * 3 + $n2 * 4 + $n1 * 5;
        $d1 = 11 - (self::mod($d1, 11));
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n12 * 3 + $n11 * 4 + $n10 * 5 + $n9 * 6 + $n8 * 7 + $n7 * 8 + $n6 * 9 + $n5 * 2 + $n4 * 3 + $n3 * 4 + $n2 * 5 + $n1 * 6;
        $d2 = 11 - (self::mod($d2, 11));
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . "." . $n3 . $n4 . $n5 . "." . $n6 . $n7 . $n8 . "/" . $n9 . $n10 . $n11 . $n12 . "-" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $n10 . $n11 . $n12 . $d1 . $d2;
        }
        return $retorno;
    }

    /**
     * Método para gerar CPF válido, com máscara ou não
     * @example cpfRandom(0) (para retornar CPF sem máscara)
     * @param int $mascara
     * @return string
     */
    public function cpfRandom($mascara = "1")
    {
        $n1 = rand(0, 9);
        $n2 = rand(0, 9);
        $n3 = rand(0, 9);
        $n4 = rand(0, 9);
        $n5 = rand(0, 9);
        $n6 = rand(0, 9);
        $n7 = rand(0, 9);
        $n8 = rand(0, 9);
        $n9 = rand(0, 9);
        $d1 = $n9 * 2 + $n8 * 3 + $n7 * 4 + $n6 * 5 + $n5 * 6 + $n4 * 7 + $n3 * 8 + $n2 * 9 + $n1 * 10;
        $d1 = 11 - (self::mod($d1, 11));
        if ($d1 >= 10) {
            $d1 = 0;
        }
        $d2 = $d1 * 2 + $n9 * 3 + $n8 * 4 + $n7 * 5 + $n6 * 6 + $n5 * 7 + $n4 * 8 + $n3 * 9 + $n2 * 10 + $n1 * 11;
        $d2 = 11 - (self::mod($d2, 11));
        if ($d2 >= 10) {
            $d2 = 0;
        }
        $retorno = '';
        if ($mascara == 1) {
            $retorno = '' . $n1 . $n2 . $n3 . "." . $n4 . $n5 . $n6 . "." . $n7 . $n8 . $n9 . "/" . $d1 . $d2;
        } else {
            $retorno = '' . $n1 . $n2 . $n3 . $n4 . $n5 . $n6 . $n7 . $n8 . $n9 . $d1 . $d2;
        }
        return $retorno;
    }

/**
 * @param type $dividendo
 * @param type $divisor
 * @return type
 */
    private static function mod($dividendo, $divisor)
    {
        return round($dividendo - (floor($dividendo / $divisor) * $divisor));
    }

}
