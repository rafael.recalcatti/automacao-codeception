<?php

namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Codeception\Exception\ModuleConfigException;
use Codeception\Module as CodeceptionModule;
use Codeception\Module\Db;
use Codeception\Module\WebDriver;
use Symfony\Component\Yaml\Yaml;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class AmbienteHelper extends CodeceptionModule
{
    const NOME_ARQUIVO_CONFIG = 'arquivoConfig';

    private $_contextoAtual;
    /** @var array */
    private $contextos;

    public function _initialize()
    {
        //todo pegar os dados ler do param
        //todo tem que haver alguma dependencia com (phpbrowser ou webdriver) e DB
        if (!isset($this->config[self::NOME_ARQUIVO_CONFIG])) {
            throw new ModuleConfigException(get_class($this), sprintf(
                'Parameter "%s" is not specified in module\'s configurations.',
                self::NOME_ARQUIVO_CONFIG
            ));
        }

        $nomeArquivo = $this->config[self::NOME_ARQUIVO_CONFIG];
        $contextos = Yaml::parseFile($nomeArquivo)['contextos'];
        $this->contextos = $contextos;
        print_r($contextos[array_keys($contextos)[0]]['url']);
        /** @var WebDriver $webDriver */
        $webDriver = $this->getModule('WebDriver');
        $webDriver->_reconfigure(['url' => $contextos[array_keys($contextos)[0]]['url']]);
        parent::_initialize(); // TODO: Change the autogenerated stub
    }

    public function setAmbiente($contexto)
    {
        $contexto = strtoupper($contexto);

        if (!preg_match('/[1-2]G_(RS|SC|PR)/', $contexto)) {

            $urlWiki = 'http://'; //$this->contextos['wiki']['url'];
            throw new \Exception("Argumento contexto passado ({$contexto}), está em formato inválido. Consulte a Wiki de paramêtros de Features." . " URL:{$urlWiki}");

        } else {

            if (!in_array($contexto, array_keys($this->contextos))) {
                throw new \Exception("Contexto inválido: [$contexto]");
            }
            /** @var  WebDriver $webdriver */
            $webdriver = $this->getModule('WebDriver');
            $webdriver->_reconfigure(['url' => $this->contextos[$contexto]['url']]);

            /** @var Db $db */
            //Configurando conexões DB
            try {

                $db = $this->getModule('Db');
                //print_r(">>>>>>DB BEFORE RECONFIGURE<<<<<<<<\n");
                //print_r($db->_getDriver());
                $db->__destruct();
                $db->_reconfigure($this->contextos[$contexto]['db']);
                $db->_initialize();
                //print_r(">>>>>>DB AFTER RECONFIGURE<<<<<<<<\n");
                //print_r($db->_getDriver());
                $this->setContextoAtual($contexto);

            } catch (\PDOException $e) {
                throw new \ModuleException("Contexto DB inválido: [$contexto]\n Verifique o arquivo de configuração:contextos.yml");
            }
        }
    }

    public function getUrlBaseAtual()
    {
        /** @var  WebDriver $webdriver */
        $webdriver = $this->getModule('WebDriver');
        return $webdriver->_getUrl();
    }

    public function getContextoAtual()
    {
        if (!$this->_contextoAtual) {
            throw new \Exception('Ambiente nao definido');
        }
        return $this->_contextoAtual;
    }

    private function setContextoAtual($contexto)
    {
        $this->_contextoAtual = $contexto;
    }   
   
}
