<?php
use \Codeception\Events;
use Symfony\Component\EventDispatcher\Event;

class MyCustomExtension extends \Codeception\Extension
{
    // list events to listen to
    // Codeception\Events constants used to set the event

    public static $events = array(
        Events::SUITE_AFTER => 'afterSuite',
        Events::SUITE_BEFORE => 'beforeSuite',
        Events::TEST_BEFORE => 'beforeTest',
        Events::TEST_AFTER => 'afterTest',
        Events::STEP_BEFORE => 'beforeStep',
        Events::TEST_FAIL => 'testFailed',
        Events::RESULT_PRINT_AFTER => 'print',
        Events::TEST_ERROR => 'testError'
    );

    // methods that handle events

    public function afterSuite(\Codeception\Event\SuiteEvent $e)
    {
        //echo "\n>>>>>AFTER SUITE<<<<<\n";
    }

    public function afterTest(\Codeception\Event\TestEvent $e, $x, $c = null)
    {
        //echo "\n>>>>>AFTER TEST<<<<<\n";
    }
    public function beforeTest(\Codeception\Event\TestEvent $e, $x, $c = null)
    {
        //echo "\n>>>>>BEFORE TEST<<<<<\n";
    }

    public function beforeSuite(\Codeception\Event\SuiteEvent $e)
    {
        //echo "\n>>>>>BEFORE SUITE<<<<<\n";
    }

    public function beforeStep(\Codeception\Event\StepEvent $e)
    {
        //echo "\n>>>>>BEFORE STEP<<<<<\n";
    }

    public function testFailed(\Codeception\Event\FailEvent $e)
    {
        echo "\n>>>>>TESTE FAILED<<<<\n";
    }

    public function testError(\Codeception\Event\FailEvent $e)
    {
        echo "\n>>>>>TESTE ERROR<<<<\n";
    }

    function print(\Codeception\Event\PrintResultEvent $e)
    {
        echo "\n>>>>>PRINT EVENT<<<<\n";
        (new \Helper\UtilHelper())->loadXMLResult();
    }
}
