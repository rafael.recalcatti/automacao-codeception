<?php
namespace Step\Acceptance\Peticao;

use TRF4\EprocPageObjects\PageObjects\Peticao\PeticaoInicialActions;

class PeticaoInicialSteps extends \AcceptanceTester
{

    protected $peticao;

    public function _inject(PeticaoInicialActions $peticao)
    {

        $this->peticao = $peticao;

    }

    /**
     * @When cadastro a petição inicial de :tipo ate a etapa :etapa
     */
    public function euCadastroAPetiçãoInicialTipo(string $tipo, int $etapa)
    {
        $this->peticao->preencheInformacoesProcesso($this, $tipo)
            ->preencheAssuntoProcesso($this, $tipo, $etapa >= 2)
            ->preenchePartesRequerentesProcesso($this, $tipo, $etapa >= 3)
            ->preenchePartesRequeridosProcesso($this, $tipo, $etapa >= 4)
            ->selecionaDocumento($this, $tipo, $etapa >= 5);
    }

    /**
     * @When cadastro com a classe :classe na competencia :competencia com assunto :assunto
     */
    public function cadastroComAClasseNaCompetenciaComAssunto($classe, $competencia, $assunto)
    {

        $this->peticao->preencheInformacoesProcesso($this, null, $classe)
            ->preencheAssuntoProcesso($this, null, true, $competencia, $assunto)
            ->preenchePartesRequerentesProcesso($this, null, true)
            ->preenchePartesRequeridosProcesso($this, null, true)
            ->selecionaDocumento($this, null, true);
    }

    /**
     * @Then devo receber o valor de custas de :valorCustas
     */
    public function euDevoReceberValorCustas($valorCustas)
    {
        $this->peticao->validarValorCustas($this, $valorCustas);

        //$this->wait(1);
    }

    /**
     * @Then devo ver o processo distribuido
     */
    public function euDevoVerOProcessoDistribuido()
    {
        $this->peticao->validarDistribuicaoPeticao($this);
        //$this->wait(3);
    }

}
