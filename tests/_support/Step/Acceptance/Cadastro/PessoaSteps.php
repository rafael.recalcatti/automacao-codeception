<?php
namespace Step\Acceptance\Cadastro;

use TRF4\EprocPageObjects\PageObjects\Cadastro\PessoaActions;


class PessoaSteps extends \AcceptanceTester
{
    
    protected $pessoa;

    public function _inject(PessoaActions $pessoa) {        
        $this->pessoa = $pessoa;        
    }
    
    /**
     * @When insiro um cpf valido e consulto
     */
    public function euInsiroUmCpfValidoEConsulto()
    {               
        $this->pessoa->consultarPessoaCPF($this);
    }

    /**
     * @When confirmo o novo cadastro
     */
    public function confirmoNovoCadastro()
    {
        $this->acceptPopup();
        $this->wait(2);
    }

    /**
     * @When insiro os dados da pessoa
     */
    public function insiroDadosDaPessoa()
    {          
        $this->pessoa->registrarPessoa($this, 'fisica');       
    }

}
