<?php
namespace Step\Acceptance\Menu;

use Page\Menu\MenuPageActions as pgMenuAct;
use TRF4\EprocPageObjects\PageObjects\Menu\MenuActions;
use Helper\UtilHelper as util;


class MenuSteps extends \AcceptanceTester
{

    protected $action;

    public function _inject(MenuActions $action) { 

        $this->action = $action;        
    }
    /**
     * @Given que tenha acessado o menu :menu
     */
    public function queEuTenhaAcessadoOMenu($menu)
    {                     
        $this->action->acessaMenu($this, $menu);        
    }

}