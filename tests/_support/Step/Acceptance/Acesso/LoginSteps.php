<?php
namespace Step\Acceptance\Acesso;

use TRF4\EprocPageObjects\PageObjects\Acesso\LoginActions;
use Codeception\Module\WebDriver;
use Helper\AmbienteHelper;
use Helper\MailHelper;

class LoginSteps extends \AcceptanceTester
{
    
    protected $action;

    public function _inject(LoginActions $action) {        
        $this->action = $action;        
    }
    
    /**
     * @Given que tenha acessado com perfil :perfil no ambiente :ambiente
     */
    public function queEuTenhaAcessadoComPerfil($perfil, $ambiente)
    {
        (new \Helper\UtilHelper())->loadXMLResult();                   
        $this->action->logarEproc($this, $ambiente, $perfil);
        $this->action->selecionarPerfil($this, null);
    }

    /**
     * @Given que tenha logado no sistema no ambiente :ambiente usando o usuario :usuario e senha :senha
     */
    public function queTenhaLogadoSistemaUsandoUsuario($ambiente, $usuario, $senha)
    {                
        $this->action->logarEproc($this, $ambiente, null, $usuario, $senha);
    }

    /**
     * @Given seleciono o perfil :perfil
     */
    public function selecionoPerfil($perfil)
    {
        $this->action->selecionarPerfil($this, $perfil);
    }

    /**
     * @Then devo ver a tela :tela
     */
    public function euDevoVerATelaPainel($tela)
    {
        $this->action->validaPainelUsuario($this, $tela);
    }

}
