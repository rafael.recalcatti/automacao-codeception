<?php

namespace TRF4\EprocPageObjects\PageObjects\Menu;

use Codeception\Module;

class MenuActions extends Module
{
/**
 * Acessa o menu ou submenu passado como parâmetro. Ex.: 'Menu Textual', 'Mensagens/Caixa de Mensagens'.
 * OBS.: Passar o nome do menu igual o contido na página
 * @param $menu
 * @return void
 */
    public static function acessaMenu($actor, $menu)
    {

        if (stripos($menu, '/')) {

  
            $xpathMenu = '//div[@id="divInfraAreaTelaE"]//a[text()="' . explode('/', $menu)[0] . '"]/..';
            $actor->waitForElement($xpathMenu, 5);
            $actor->higthline($actor, $xpathMenu);
            //$actor->moveMouseOver($xpathMenu, '1', '1');           

            $xpathSubMenu = '//div[@id="divInfraAreaTelaE"]//a[text()="' . explode('/', $menu)[0] . '"]/../ul//a[text()="' . explode('/', $menu)[1] . '"]';
            $actor->clickJS($actor, $xpathSubMenu);           

        } else {
           
            $xpathMenu = '//div[@id="divInfraAreaTelaE"]//a[text()="' . $menu . '"]/..';
            $actor->waitForElement($xpathMenu, 5);
            $actor->higthline($actor, $xpathMenu);
            //$actor->moveMouseOver($xpathMenu, '1', '1');
            $actor->click($xpathMenu);
        }

    }

}
