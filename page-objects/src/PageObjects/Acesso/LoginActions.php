<?php

namespace TRF4\EprocPageObjects\PageObjects\Acesso;


use TRF4\EprocPageObjects\PageObjects\Acesso\LoginPage;

class LoginActions extends LoginElementsMap
{

    protected $page;

    public function _inject(LoginPage $page) {
               
        $this->page = $page;        
    }

    /**
     * Loga no sistema passando como parâmetro
     * o usuário e senha e a instancia/actor do driver
     * @param $actor
     * @param $ambiente
     * @param $usuario
     * @param $senha
     * @return void
     *
     */
    public function logarEproc($actor, $ambiente, $perfil = null, $usuario = null, $senha = null)
    {
                      
        $actor->setAmbiente($ambiente);

        if($usuario == null && $senha == null){

            $classPage = $this->page->loadDataPage($ambiente, $perfil);
            $usuario = $classPage->usuario;
            $senha =  $classPage->senha;

         }
        

        try
        {            
            $actor->amOnPage('/');
            $actor->higthline($actor, self::INPUT_USUARIO, self::COLOR_GREEN);
            $actor->waitForElementVisible(self::INPUT_USUARIO, 15);
            $actor->seeElement(self::INPUT_USUARIO);
            $actor->higthline($actor, self::INPUT_USUARIO, self::COLOR_GREEN);
            $actor->fillField(self::INPUT_USUARIO, $usuario);

        } catch (\Throwable | \Error | \Exception $e) {

            echo 'Erro ao inserir o usuário.' . $e . '\n';
        }

        try
        {
            $actor->seeElement(self::INPUT_SENHA);
            $actor->higthline($actor, self::INPUT_SENHA, self::COLOR_GREEN);
            $actor->fillField(self::INPUT_SENHA, $senha);

        } catch (\Throwable | \Error | \Exception $e) {

            echo 'Erro ao inserir a senha.';
        }

        try
        {
            $actor->seeElement(self::BUTTON_ENTRAR);
            $actor->higthline($actor, self::BUTTON_ENTRAR, self::COLOR_GREEN);
            $actor->click(self::BUTTON_ENTRAR);

        } catch (\Throwable | \Error | \Exception $e) {

            echo 'Erro ao clicar em entra no sistema.';
        }

    }

    /**
     * Seleciona o perfil do usuário logado, passando como parâmetro. Se null seleciona o primeiro perfil
     * o texto da lotação que se deseja atuar e a instancia/actor do driver
     * @param $actor
     * @param $perfil
     * @return void
     */
    public static function selecionarPerfil($actor, $perfil)
    {
        try
        {

            $elePerfil = $perfil == null ? '//input[@class="infraRadio"]/../../td[text()!="' . $perfil . '"]/..//td/input/..' : '//input[@class="infraRadio"]/../../td[text()="' . $perfil . '"]/..//td/input/..';
            $actor->waitForElementVisible($elePerfil, 2);
            $actor->seeElement($elePerfil);
            $actor->higthline($actor, $elePerfil, self::COLOR_GREEN);
            $actor->click($elePerfil);

        } catch (\Throwable | \Error | \Exception $e) {

            echo 'Step não aplica à este cenário. Ou então parâmetro passado inválido.';
        }
    }

    /**
     * Valida a tela do usuário logado, passando como parâmetro
     * o titulo especifico de cada tela e a instancia/actor do driver
     * @param $actor
     * @param $tipoUsuario
     * @return void
     */
    public static function validaPainelUsuario($actor, $tipoUsuario)
    {
        try
        {
            $actor->see($tipoUsuario, self::TITULO_PAINEL);
            $actor->higthline($actor, self::TITULO_PAINEL, self::COLOR_GREEN);
            $actor->makeScreenshot();

        } catch (\Throwable | \Error | \Exception $e) {
            $actor->higthline($actor, self::TITULO_PAINEL, self::COLOR_RED);
            $actor->makeScreenshot();
        }

        $actor->wait(2);
        $actor->click('#lnkSairSistema');
    }

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL . $param;
    }

}
