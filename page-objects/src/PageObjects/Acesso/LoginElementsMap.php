<?php

namespace TRF4\EprocPageObjects\PageObjects\Acesso;

use Codeception\Module;

class LoginElementsMap extends Module
{

const URL = '';
const HEADER='div#divInfraBarraTribunalE.infraBarraTribunalE';
const INPUT_USUARIO = '.infraText';
const INPUT_SENHA = '#pwdSenha';
const BUTTON_ENTRAR = '#smbEntrar';
const TITULO_PAINEL = '#divInfraBarraLocalizacao';
const COLOR_GREEN = '#00CC00';
const COLOR_RED = 'red';

}

