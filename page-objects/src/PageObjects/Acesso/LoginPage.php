<?php

namespace TRF4\EprocPageObjects\PageObjects\Acesso;

use Codeception\Module as CodeceptionModule;
use Symfony\Component\Yaml\Yaml;

class LoginPage extends CodeceptionModule
{

    const NOME_ARQUIVO_CONFIG = 'arquivoConfig';
    public $usuario;
    public $senha; 
  

     /** @var array */
     private $acessos;

     public function _initialize()
     {
 
         if (!isset($this->config[self::NOME_ARQUIVO_CONFIG])) {
             throw new ModuleConfigException(get_class($this), sprintf(
                 'Parameter "%s" is not specified in module\'s configurations.',
                 self::NOME_ARQUIVO_CONFIG
             ));
         }
 
         $nomeArquivo = $this->config[self::NOME_ARQUIVO_CONFIG];
         $acessos = Yaml::parseFile($nomeArquivo)['acesso'];
         $this->acessos = $acessos;
         
     }
 
     public function loadDataPage($contexto, $tipoAcesso)
     {
        $nameArchive = $this->config[self::NOME_ARQUIVO_CONFIG];

        if (!in_array($contexto, array_keys($this->acessos))) {
            throw new \Exception("Ambiente usuário inválido: [$contexto]. Verifique o arquivo: [$nameArchive].");
        }
        
        if (!in_array($tipoAcesso, array_keys($this->acessos[$contexto]))) {
             throw new \Exception("Tipo usuário inválido: [$tipoAcesso] no ambiente: [$contexto]. Verifique o arquivo: [$nameArchive].");
         }
                
         $arrayDados = $this->acessos[$contexto][$tipoAcesso];
 
         (new \Helper\UtilHelper())->validateAttributesPage($this, $arrayDados, $this->config[self::NOME_ARQUIVO_CONFIG]);
 
         foreach ($arrayDados as $k => $v) {
 
             $this->$k = $v;
 
         }         
         return $this;
     }    

}
