<?php
namespace TRF4\EprocPageObjects\PageObjects\Peticao;

use Codeception\Module;

class PeticaoInicialElementsMap extends Module
{
 
    //Imagem de carregamento
    const IMG_LOADING = '#imgInfraAviso';

    //Elementos Passo 1
    const COMBO_LOCALIDADE_JUDICIAL = '#selLocalidadeJudicial';
    const COMBO_RITO = '#selRitoProcesso';
    const COMBO_CLASSE = '#selIdClasseJudicial';
    const COMBO_AREA = '#selIdGrupoCompetencia';
    const COMBO_NIVEL_SIGILO = '#selIdSigilo';
    const INPUT_PROCESSO_ORIGINARIO = '#txtProcessoOriginario';
    const INPUT_JUIZO = '#txtDescJuizo';
    const INPUT_VALOR_CAUSA = '#txtValorCausa';
    const CHECKBOX_NAO_SE_APLICA = '#chkNaoAplicaValor';
    const CHECKBOX_VALOR_ALCADA = '#chkValorAlcada';
    const BUTTON_PROXIMA = '#btnSalvar';
    const BUTTON_CANCELAR = '#btnCancelar';
    const LABEL_VALOR_CUSTA_CALCULADA = '.valorCustaCalculada';
    const COMBO_ORIGEM = '#selTipoOrigemJudicial';
    const INPUT_ORIGINARIO = '#txtProcessoOriginario';



    //Elementos Passo 2
    const COMBO_COMPETENCIA = '#selCodCompetencia';
    const INPUT_FILTRO = '#txtFiltroPesquisa';
    const CHECKBOX_ASSUNTO = '#rdoAssunto';
    const CHECKBOX_CODIGO = '#rdoCodigo';
    const CHECKBOX_GLOSSARIO = '#rdoGlossario';
    const BUTTON_PESQUISAR = '#btnPesquisar';
    const BUTTON_FILTRAR = '#btnFiltrar';
    const BUTTON_LIMPAR = '#btnLimpar';
    const BUTTON_INCLUIR_ASSUNTO = '#btnIncluirAssunto';
    const BUTTON_LIMPAR_ASSUNTO = '#btnLimparAssunto';
    const INPUT_DESCRICAO_ASSUNTO = '#txtDesAssunto';    
    const BUTTON_EXPANDIR_TODOS ='#abrirTodos';
    const ARVORE_ACOES = '//ul[@class="jstree-container-ul jstree-children jstree-no-dots"]//li';

    //Elementos Passo 3/4
    const COMBO_TIPO_PESSOA = '#selTipoPessoa';
    const INPUT_CPF_CNPJ = '#txtCpfCnpj';
    const CHECKBOX_PF_SEM_CPF = '#chkSemCpfCnpj';
    const COMBO_JUSTIFICATIVA_SEM_CPF = '#selSemCpf';
    const COMBO_OUTRO_DOC = '#selTipoDoc';
    const INPUT_NUM_DOC = '#txtNumDoc';
    const INPUT_NOME = '#txtNome';
    const COMBO_IDENTIDADE = '#selEntidade';
    const BUTTON_CONSULTAR = '#btnConsultarNome';
    const BUTTON_INCLUIR = '#btnIncluir';
    const BUTTON_INCLUIR_ENTIDADE = '#btnIncluirEnt';
    const COMBO_PRINCIPAL = '#selPrincipal0';
    const BUTTON_PROXIMA_2 = '#btnProxima';

    //Elementos Passo 5
    const UPLOAD_ARQUIVO = '//div[@class="qq-upload-button"]/input';
    const INPUT_TIPO_ARQUIVO = '#selTipoArquivo_1';
    const LABEL_LISTAR_TODOS_TIPOS_ARQUIVO = '#lblListarTipo_1';
    const BUTTON_ENVIAR_ARQUIVO = '#btnEnviarArquivos';
    const BUTTON_CONFIRMAR_AJUIZAMENTO = '#sbmConfirmar';
    const IFRAME_CONFIRMACAO_AJUIZAMENTO = 'ifrSubFrm';
    const TIPO_AQUIVO_PET_INI = '//table[@id="tbDocumentosCadastradas"]//td//span[text()="INIC"]';
    const BUTTON_SALVAR_DIST_FUTURA = '#btnSalvarDistribuicao';

    //Elementos Tela Confirmação
    const TITULO_PAGINA_CONFIRMACAO_DISTRIBUICAO = '#divDesProcessoEnviado';
    const LABEL_NUMERO_PROCESSO = '#lblDesNumProcesso';
    const LABEL_CHAVE_PROCESSO = '#lblDesChaveConsulta';
    const LABEL_MAJISTRADO_PROCESSO = '#lblDesJuiz';
    const LABEL_CLASSE = '#lblDesClasse';
    const BUTTON_GERAR_CUSTAS = '#btnGeraCustas';       
    

}
