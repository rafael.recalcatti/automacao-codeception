<?php

namespace TRF4\EprocPageObjects\PageObjects\Peticao;

use Codeception\Module as CodeceptionModule;
use Symfony\Component\Yaml\Yaml;


class PeticaoInicialPage extends CodeceptionModule
{

    const NOME_ARQUIVO_CONFIG = 'arquivoConfig';
    //Atributos Etapa 1
    public $localidade;
    public $rito;
    public $area;
    public $classe;
    public $nivel;
    public $originario;
    public $origem;
    public $juizo;
    public $valor;
    public $nao_se_aplica;
    public $valor_alcada;
    public $previsao_custas;
    private static $array = array(0 => 'Sem Sigilo (Nível 0)', 1 => 'Segredo de Justiça (Nível 1)');

    //Atributos Etapa 2
    public $assunto;
    public $competencia;
    public $acao;

    //Atributos Etapa 3/4
    public $tipo_polo_ativo;
    public $identidade_polo_ativo;
    public $tipo_polo_passivo;
    public $identidade_polo_passivo;

    //Atributos Etapa 3/4
    public $tipoDocumento;

    /** @var array */
    private $peticoes;


    public function _initialize()
    {

        if (!isset($this->config[self::NOME_ARQUIVO_CONFIG])) {
            throw new ModuleConfigException(get_class($this), sprintf(
                'Parameter "%s" is not specified in module\'s configurations.',
                self::NOME_ARQUIVO_CONFIG
            ));
        }

        $nomeArquivo = $this->config[self::NOME_ARQUIVO_CONFIG];
        $peticoes = Yaml::parseFile($nomeArquivo)['peticao'];
        $this->peticoes = $peticoes;

    }

    public function getNivel($index = 0)
    {
        $_return;
        try {

            $_return = array_key_exists($index, self::$array) ? self::$array[$index] : self::$array[0];

        } catch (\Throwable | \Error | \Exception $e) {

            $_return = self::$array[0];

        }
        return $_return;
    }
   

    public function loadDataPage($contexto, $tipoPeticao)
    {
        $nameArchive = $this->config[self::NOME_ARQUIVO_CONFIG];

        if (!in_array($contexto, array_keys($this->peticoes))) {
            throw new \Exception("Ambiente petição inválido: [$contexto]. Verifique o arquivo: [$nameArchive]");
        }

        if (!in_array($tipoPeticao, array_keys($this->peticoes[$contexto]))) {
            $nameArchive = $this->config[self::NOME_ARQUIVO_CONFIG];
            throw new \Exception("Tipo petição inválido: [$tipoPeticao] no contexto: [$contexto]. Verifique o arquivo: [$nameArchive]. ");
        }

        $arrayDados = $this->peticoes[$contexto][$tipoPeticao];

        (new \Helper\UtilHelper())->validateAttributesPage($this, $arrayDados, $this->config[self::NOME_ARQUIVO_CONFIG]);

        foreach ($arrayDados as $k => $v) {

            $this->$k = $v;
        }

        return $this;
    }

}
