<?php

namespace TRF4\EprocPageObjects\PageObjects\Peticao;

use Helper\UtilHelper as util;
use TRF4\EprocPageObjects\PageObjects\Peticao\PeticaoInicialPage;

class PeticaoInicialActions extends PeticaoInicialElementsMap
{

    protected $classPage;
    protected $page;

    public function _inject(PeticaoInicialPage $page)
    {

        $this->page = $page;
    }

    public function preencheInformacoesProcesso($actor, $tipo, $classe = null)
    {
        //Cadastro Informações Processo
        $contexto = $actor->getContextoAtual();
        $this->classPage = $this->page->loadDataPage($contexto, $tipo);
        $classPage = $this->classPage;
        $actor->selectOption(self::COMBO_LOCALIDADE_JUDICIAL, $classPage->localidade);
        $actor->higthline($actor, self::COMBO_LOCALIDADE_JUDICIAL);
        //$actor->wait(1);

        try {

            $actor->selectOption(self::COMBO_RITO, $classPage->rito);
            $actor->higthline($actor, self::COMBO_RITO);

        } catch (\Throwable | \Error | \Exception $e) {

        }

        $actor->selectOption(self::COMBO_AREA, $classPage->area);
        $actor->higthline($actor, self::COMBO_AREA);
        //$actor->wait(1);

        $actor->selectOption(self::COMBO_CLASSE, $classe == null ? $classPage->classe : $classe);
        $actor->higthline($actor, self::COMBO_CLASSE);

        $actor->selectOption(self::COMBO_NIVEL_SIGILO, $classPage->getNivel($classPage->nivel));
        $actor->higthline($actor, self::COMBO_NIVEL_SIGILO);
        //$actor->wait(1);

        if (!empty($classPage->origem)) {
            $actor->selectOption(self::COMBO_ORIGEM, $classPage->origem);
            $actor->higthline($actor, self::COMBO_ORIGEM);
        }

        if (!empty($classPage->originario)) {
            $actor->selectOption(self::INPUT_ORIGINARIO, $classPage->originario);
            $actor->higthline($actor, self::INPUT_ORIGINARIO);
        }

        //$actor->wait(1);

        if ($classPage->nao_se_aplica == '1') {

            $actor->higthline($actor, self::CHECKBOX_NAO_SE_APLICA);
            $actor->click(self::CHECKBOX_NAO_SE_APLICA);
            $actor->higthline($actor, self::INPUT_VALOR_CAUSA);
            $actor->wait(3);

        } else if ($classPage->valor_alcada == '1') {

            $actor->higthline($actor, self::CHECKBOX_VALOR_ALCADA);
            $actor->click(self::CHECKBOX_VALOR_ALCADA);
            $actor->higthline($actor, self::INPUT_VALOR_CAUSA);
            $actor->wait(3);

        } else {

            $actor->fillField(self::INPUT_VALOR_CAUSA, $classPage->valor);
            $actor->higthline($actor, self::INPUT_VALOR_CAUSA);
            $actor->wait(1);
        }

        return $this;

    }
    public function preencheAssuntoProcesso($actor, $tipo, bool $isExcuta, $competencia = null, $acao = null)
    {

        $classPage = $this->classPage;
        if ($isExcuta) {

            $actor->higthline($actor, self::BUTTON_PROXIMA);
            $actor->click(self::BUTTON_PROXIMA);
            //$actor->wait(1);

            $arg = $acao == null ? $classPage->acao : $acao;

            $elementAcao = '//div[@id="divArvore"]//span[text()="' . $arg . '"]';

            try {

                $actor->selectOption(self::COMBO_COMPETENCIA, $competencia == null ? $classPage->competencia : $competencia);
                $actor->higthline($actor, self::COMBO_COMPETENCIA);
                $actor->waitForElement(self::ARVORE_ACOES, 15);

            } catch (\Throwable | \Error | \Exception $e) {
            }

            $actor->waitForElementVisible(self::BUTTON_EXPANDIR_TODOS, 15);
            //$actor->higthline($actor, self::BUTTON_EXPANDIR_TODOS);
            //$actor->clickJS($actor, self::BUTTON_EXPANDIR_TODOS);
            $actor->click(self::BUTTON_EXPANDIR_TODOS);

            $actor->wait(1);
            $actor->waitForElementNotVisible(self::IMG_LOADING, 15);

            $actor->higthline($actor, $elementAcao);
            $actor->wait(2);
            $actor->clickJS($actor, $elementAcao);
            $actor->waitForElementNotVisible(self::IMG_LOADING, 15);

            $actor->higthline($actor, self::BUTTON_INCLUIR_ASSUNTO);
            $actor->click(self::BUTTON_INCLUIR_ASSUNTO);

        }

        return $this;

    }
    public function preenchePartesRequerentesProcesso($actor, $tipo, bool $isExcuta)
    {
        $classPage = $this->classPage;
        if ($isExcuta) {

            //$actor->wait(1);

            $actor->higthline($actor, self::BUTTON_PROXIMA);
            $actor->click(self::BUTTON_PROXIMA);

            //print_r($classPage);

            $actor->higthline($actor, self::COMBO_TIPO_PESSOA);
            $actor->selectOption(self::COMBO_TIPO_PESSOA, $classPage->tipo_polo_ativo);

            if ($classPage->tipo_polo_ativo != 'Entidade') {

                $identidadeRetornada = '//table[@id="tblResultadosBusca"]//td[text()="' . $actor->mask('###.###.###-##', $classPage->identidade_polo_ativo) . '"]';

                $actor->higthline($actor, self::INPUT_CPF_CNPJ);
                $actor->fillField(self::INPUT_CPF_CNPJ, $classPage->identidade_polo_ativo);

                $actor->click(self::BUTTON_CONSULTAR);
                $actor->waitForElement($identidadeRetornada, 5);

                $actor->higthline($actor, self::BUTTON_INCLUIR);
                $actor->click(self::BUTTON_INCLUIR);

            } else {

                $actor->selectOption(self::COMBO_IDENTIDADE, $classPage->identidade_polo_ativo);
                $actor->higthline($actor, self::COMBO_IDENTIDADE);

                $actor->higthline($actor, self::BUTTON_INCLUIR_ENTIDADE);
                $actor->click(self::BUTTON_INCLUIR_ENTIDADE);

            }

            //$actor->wait(1);

        }
        return $this;

    }

    public function preenchePartesRequeridosProcesso($actor, $tipo, bool $isExcuta)
    {
        $classPage = $this->classPage;
        if ($isExcuta) {

            //$actor->wait(1);

            $actor->higthline($actor, self::BUTTON_PROXIMA_2);
            $actor->click(self::BUTTON_PROXIMA_2);

            //print_r($classPage);

            $actor->higthline($actor, self::COMBO_TIPO_PESSOA);
            $actor->selectOption(self::COMBO_TIPO_PESSOA, $classPage->tipo_polo_passivo);

            if ($classPage->tipo_polo_passivo != 'Entidade') {

                $identidadeRetornada = '//table[@id="tblResultadosBusca"]//td[text()="' . $actor->mask('###.###.###-##', $classPage->identidade_polo_passivo) . '"]';

                $actor->higthline($actor, self::INPUT_CPF_CNPJ);
                $actor->fillField(self::INPUT_CPF_CNPJ, $classPage->identidade_polo_passivo);

                $actor->click(self::BUTTON_CONSULTAR);
                $actor->waitForElement($identidadeRetornada, 5);

                $actor->higthline($actor, self::BUTTON_INCLUIR);
                $actor->click(self::BUTTON_INCLUIR);

            } else {

                $actor->selectOption(self::COMBO_IDENTIDADE, $classPage->identidade_polo_passivo);
                $actor->higthline($actor, self::COMBO_IDENTIDADE);

                $actor->higthline($actor, self::BUTTON_INCLUIR_ENTIDADE);
                $actor->click(self::BUTTON_INCLUIR_ENTIDADE);

            }

            //$actor->wait(1);

        }
        return $this;

    }
    public function selecionaDocumento($actor, $tipo, bool $isExcuta)
    {
        $classPage = $this->classPage;
        if ($isExcuta) {

            $actor->higthline($actor, self::BUTTON_PROXIMA_2);
            $actor->click(self::BUTTON_PROXIMA_2);

            //print_r($classPage);
            $actor->wait(1);

            try {

                $actor->dontSeeElement(self::TIPO_AQUIVO_PET_INI);
                print_r("\n>>>>>>>>>>>>PASSOU NA VERIFICAÇÃO<<<<<<<<<<<<<<\n");
                //$actor->wait(10);
                $actor->setValueJS($actor, self::INPUT_TIPO_ARQUIVO, '1');
                $actor->higthline($actor, self::BUTTON_ENVIAR_ARQUIVO);
                $actor->fillField(self::UPLOAD_ARQUIVO, $actor->getDirectoryCurrent(2) . '\_data\.PDF de teste.pdf');
                //$actor->click(self::BUTTON_ENVIAR_ARQUIVO);
                $actor->wait(2);
                $actor->clickJS($actor, self::BUTTON_ENVIAR_ARQUIVO);
                $actor->waitForElement(self::TIPO_AQUIVO_PET_INI, 15);

            } catch (\Throwable | \Error | \Exception $e) {

                print_r("\n>>>>>>>>>DOCUMENTO PETICAO INICIAL JA EXISTENTE<<<<<<<<<<<<<\n");
            }

            $actor->higthline($actor, self::BUTTON_PROXIMA);
            $actor->wait(2);
            $actor->click(self::BUTTON_PROXIMA);

            //$actor->higthline($actor, self::BUTTON_SALVAR_DIST_FUTURA);
            $actor->wait(2);
            //$actor->click(self::BUTTON_SALVAR_DIST_FUTURA);

            $actor->switchToIFrame(self::IFRAME_CONFIRMACAO_AJUIZAMENTO);
            $actor->waitForElement(self::BUTTON_CONFIRMAR_AJUIZAMENTO, 15);
            $actor->higthline($actor, self::BUTTON_CONFIRMAR_AJUIZAMENTO);
            $actor->click(self::BUTTON_CONFIRMAR_AJUIZAMENTO);

            $actor->wait(1);

        }
        return $this;

    }
    public function validarDistribuicaoPeticao($actor)
    {

        $actor->waitForElement(self::TITULO_PAGINA_CONFIRMACAO_DISTRIBUICAO, 5);

        $actor->higthline($actor, self::TITULO_PAGINA_CONFIRMACAO_DISTRIBUICAO);
        $actor->see('Processo Distribuído.', self::TITULO_PAGINA_CONFIRMACAO_DISTRIBUICAO);

        $actor->higthline($actor, self::LABEL_MAJISTRADO_PROCESSO);
        //$actor->assertContains('Processo Distribuído.', self::LABEL_MAJISTRADO_PROCESSO, 'NÃO ENCONTROU O TEXTO NO ELEMETO');

        $actor->higthline($actor, self::LABEL_NUMERO_PROCESSO);
        $numeroProcesso = str_replace("-", "", str_replace(".", "", $actor->grabTextFrom(self::LABEL_NUMERO_PROCESSO)));
        $juizoProcesso = $actor->grabTextFrom(self::LABEL_MAJISTRADO_PROCESSO);

        echo "\n>>>>>>>>>>>>>NUMERO PROCESSO:::{$numeroProcesso}\n";
        echo "\n>>>>>>>>>>>>>JUIZO PROCESSO:::{$juizoProcesso}\n";

        $actor->seeInDatabase('processo', array('num_processo' => $numeroProcesso));
        //$actor->seeNumRecords(1, 'processo', array('num_processo' => $numeroProcesso));

        $rows = $actor->grabNumRecords('processo', array('num_processo' => $numeroProcesso));
        echo "\n>>>>>NUMBER ROWS INSERTED:::{$rows}\n";
        $actor->makeScreenshot();
        $actor->wait(3);

        return $this;
    }
    public function validarValorCustas($actor, $valorCustas)
    {
        try {

            $actor->see($valorCustas, self::LABEL_VALOR_CUSTA_CALCULADA);
            $actor->higthline($actor, self::LABEL_VALOR_CUSTA_CALCULADA);
            $actor->makeScreenshot();

        } catch (\Throwable | \Error | \Exception $e) {

            $actor->higthline($actor, self::LABEL_VALOR_CUSTA_CALCULADA, 'RED');
            $actor->see($valorCustas, self::LABEL_VALOR_CUSTA_CALCULADA);

        }

    }
}
