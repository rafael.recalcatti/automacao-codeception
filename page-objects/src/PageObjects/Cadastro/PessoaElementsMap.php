<?php

namespace TRF4\EprocPageObjects\PageObjects\Cadastro;

use Codeception\Module;

class PessoaElementsMap extends Module
{

const INPUT_CPF = '#txtCPF';
const INPUT_SIGLA = '#txtSiglaOab';
const INPUT_NOME = '#txtNome';
const COMBO_SEXO = '#selSexo';
const COMBO_ESTADO_CIVIL = '#selEstCivil';
const COMBO_NACIONALIDADE = '#selNacionalidade';
const COMBO_NATURALIDADE = '#selUfNaturalidade';
const COMBO_CIDADE_NATURALIDADE = '#selLocalidadeNaturalidade';
const INPUT_DATA_NASCIMENTO = '#txtDataNascimento';
const INPUT_PROFISSAO = '#txtProfissao';
const INPUT_PAI = '#txtProfissao';
const INPUT_MAE = '#txtProfissao';
const COMBO_TIPO_ENDERECO = '#selTipoEnd';
const INPUT_CEP = '#txtCep';
const INPUT_NUMERO = '#txtEndNum';
const BUTTON_INCLUIR_END = '#btnIncEnd';
const BUTTON_SALVAR_PESSOA = '//div[@id="divInfraBarraComandosSuperior"]//button[@name="sbmSalvarPessoa"]';
const BUTTON_BUSCAR_CEP = '#txtImage';
const BUTTON_CONSULTAR_PESSOA = '#btnConsultar';



}