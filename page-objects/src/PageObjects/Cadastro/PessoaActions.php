<?php

namespace TRF4\EprocPageObjects\PageObjects\Cadastro;

use TRF4\EprocPageObjects\PageObjects\Cadastro\PessoaPage;
use Helper\UtilHelper as util;

class PessoaActions extends PessoaElementsMap
{
    
    protected $page;

    public function _inject(PessoaPage $page) {
               
        $this->page = $page;        
    }

/**
 * Insere um cpf valido e efetua a consulta
 * @param $actor
 * @param $arg
 * @return void
 */
    public static function consultarPessoaCPF($actor, $cpf = null)
    {

        $cpf = $cpf == null ? $actor->cpfRandom(0) : $cpf;
        echo "\n>>>>CPF CADASTRADO:::{$cpf}\n";
        $actor->fillField(self::INPUT_CPF, $cpf);
        $actor->click(self::BUTTON_CONSULTAR_PESSOA);

    }

/**
 * Insere os dados da pessoa a ser cadastrada
 * @param $actor
 * @param $objectClass
 * @return void
 */
    public function registrarPessoa($actor, $tipo)
    {
      
        $pageClass = $this->page->loadDataPage($tipo);

        $actor->higthline($actor, self::INPUT_NOME);
        $actor->fillField(self::INPUT_NOME, $pageClass->nome);

        $actor->higthline($actor, self::COMBO_SEXO);
        $actor->selectOption(self::COMBO_SEXO, $pageClass->sexo);

        $actor->higthline($actor, self::COMBO_ESTADO_CIVIL);
        $actor->selectOption(self::COMBO_ESTADO_CIVIL, $pageClass->estadoCivil);

        $actor->higthline($actor, self::INPUT_DATA_NASCIMENTO);
        $actor->fillField(self::INPUT_DATA_NASCIMENTO, $pageClass->dataNascimento);

        $actor->higthline($actor, self::INPUT_PROFISSAO);
        $actor->fillField(self::INPUT_PROFISSAO, $pageClass->profissao);

        $actor->higthline($actor, self::COMBO_NATURALIDADE);
        $actor->selectOption(self::COMBO_NATURALIDADE, $pageClass->naturalidade);

        $actor->higthline($actor, self::COMBO_CIDADE_NATURALIDADE);
        $actor->selectOption(self::COMBO_CIDADE_NATURALIDADE, $pageClass->cidadeNaturalidade);

        $actor->higthline($actor, self::INPUT_MAE);
        $actor->fillField(self::INPUT_MAE, $pageClass->mae);

        $actor->higthline($actor, self::INPUT_PAI);
        $actor->fillField(self::INPUT_PAI, $pageClass->pai);

        $actor->higthline($actor, self::INPUT_CEP);
        $actor->fillField(self::INPUT_CEP, $pageClass->cep);

        $actor->wait(1);

        $actor->higthline($actor, self::BUTTON_BUSCAR_CEP);
        $actor->clickJS($actor, self::BUTTON_BUSCAR_CEP);
        //$actor->click(self::BUTTON_BUSCAR_CEP);

        $actor->wait(2);

        $actor->higthline($actor, self::INPUT_NUMERO);
        $actor->fillField(self::INPUT_NUMERO, $pageClass->numero);

        $actor->click(self::BUTTON_INCLUIR_END);
        
        $actor->higthline($actor,self::BUTTON_SALVAR_PESSOA);
        $actor->click(self::BUTTON_SALVAR_PESSOA);
        
        $actor->wait(1);                

    }

}
