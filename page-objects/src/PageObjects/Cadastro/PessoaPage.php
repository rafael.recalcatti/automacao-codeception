<?php

namespace TRF4\EprocPageObjects\PageObjects\Cadastro;

use Codeception\Module as CodeceptionModule;
use Symfony\Component\Yaml\Yaml;

class PessoaPage extends CodeceptionModule
{

    const NOME_ARQUIVO_CONFIG = 'arquivoConfig';
    public $nome;
    public $sexo;
    public $estadoCivil;
    public $dataNascimento;
    public $profissao;
    public $naturalidade;
    public $cidadeNaturalidade;
    public $pai;
    public $mae;
    public $cep;
    public $numero;
 

    /** @var array */
    private $pessoas;

    public function _initialize()
    {

        if (!isset($this->config[self::NOME_ARQUIVO_CONFIG])) {
            throw new ModuleConfigException(get_class($this), sprintf(
                'Parameter "%s" is not specified in module\'s configurations.',
                self::NOME_ARQUIVO_CONFIG
            ));
        }

        $nomeArquivo = $this->config[self::NOME_ARQUIVO_CONFIG];
        $pessoas = Yaml::parseFile($nomeArquivo)['pessoa'];
        $this->pessoas = $pessoas;
        
    }

    public function loadDataPage($tipoPessoa)
    {
        
        if (!in_array($tipoPessoa, array_keys($this->pessoas))) {
            throw new \Exception("Tipo pessoa inválido: [$tipoPessoa]");
        }
               
        $arrayDados = $this->pessoas[$tipoPessoa];

        (new \Helper\UtilHelper())->validateAttributesPage($this, $arrayDados, $this->config[self::NOME_ARQUIVO_CONFIG]);

        foreach ($arrayDados as $k => $v) {

            $this->$k = $v;

        }
       
        return $this;
    }    
   
}
